import java.sql.*;
import java.util.ArrayList;

/**
 * Through this class, we can modify anything
 * Any button pressed is a method call, implemented so that our MainPage (The UI),
 * changes according to the current menu where we are.
 * <p>
 * Also, this controls the data flow with our database (the getting and the setting)
 */



public class Controller {
    private static Connection connection = null;

    /**
     * Here we make the connection to the Database
     *
     * @throws Exception
     */
    Controller() throws Exception {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "oop");
        System.out.println("Connection Successful!");
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void addProduct(Product newProduct) throws SQLException {
        String insertProductSQL = new String("INSERT INTO Products" +
                " VALUES (" + newProduct.getId() + ",'" + newProduct.getName() + "','" + newProduct.getCategory() + "'," +
                newProduct.getPrice() + ",'" + newProduct.getManufacturingYear() + "','" + newProduct.getWarranty() + "'," +
                newProduct.getStock() + "," + newProduct.getManufacturerId() + "," + newProduct.getCountryId() + ");");

        Statement statement = connection.createStatement();
//        System.out.println(insertProductSQL);
        statement.execute(insertProductSQL);
        System.out.println("Adding new product..." + newProduct.toString());
    }

    public static String getManufacturerById(byte id) {
        switch (id) {
            case 1:
                return Manufacturers.INTEL.getName();
            case 2:
                return Manufacturers.AMD.getName();
            case 3:
                return Manufacturers.NVIDIA.getName();
            case 4:
                return Manufacturers.LENOVO.getName();
            case 5:
                return Manufacturers.SAMSUNG.getName();
            case 6:
                return Manufacturers.PHILLIPS.getName();
            case 7:
                return Manufacturers.GENIUS.getName();
            case 8:
                return Manufacturers.ACER.getName();
            case 9:
                return Manufacturers.APPLE.getName();

        }
        System.out.println("No Manufacturer found by the id " + id);
        return null;
    }

    public static byte getIdByManufacturer(String manufacturer) {
        for (Manufacturers m : Manufacturers.values()) {
            if (m.getName().equals(manufacturer)) {
                return m.getId();
            }
        }
        System.out.println("No id found for the manufacturer " + manufacturer);
        return -1;
    }

    public static String getCountryById(byte id) {
        switch (id) {
            case 1:
                return Countries.BULGARY.getName();
            case 2:
                return Countries.GREAT_BRITAIN.getName();
            case 3:
                return Countries.GERMANY.getName();
            case 4:
                return Countries.ROMANIA.getName();
            case 5:
                return Countries.UCRAINE.getName();
            case 6:
                return Countries.ITALY.getName();
            case 7:
                return Countries.SWEDEN.getName();
            case 8:
                return Countries.JAPAN.getName();
            case 9:
                return Countries.CHINA.getName();

        }
        System.out.println("No Manufacturer found by the id " + id);
        return null;
    }

    public static byte getIdByCountry(String country) {
        for (Countries c : Countries.values()) {
            if (c.getName().equals(country)) {
                return c.getId();
            }
        }
        System.out.println("No id found for the country " + country);
        return -1;
    }

    public static Product searchProductById(byte id) throws SQLException {
        String searchProductByIdSQL = new String("select Products.name, Products.category, Products.price, Products.manufacturingDate, Products.waranty, Products.stock, Manufacturers.name, Countries.name\n" +
                "from Products\n" +
                "inner join Manufacturers\n" +
                "on (Products.manufacturerId=Manufacturers.id)\n" +
                "inner join Countries\n" +
                "on (Products.countryId=Countries.id)\n" +
                "where Products.id in " + "(" + id + ");");

        Statement statement = connection.createStatement();
        ResultSet resultSet;
        Product returnProduct;

        if (!statement.executeQuery(searchProductByIdSQL).wasNull()) {
            //method to show data of rs
            resultSet = statement.executeQuery(searchProductByIdSQL);
            resultSet.first();

            String productName = resultSet.getString("Products.name");
            String productCategory = resultSet.getString("Products.category");
            short productPrice = resultSet.getShort("Products.price");
            Date productManufacturingDate = resultSet.getDate("Products.manufacturingDate");
            Date productWarrantyDate = resultSet.getDate("Products.waranty");
            short productStock = resultSet.getShort("Products.stock");
            String productManufacturer = resultSet.getString("Manufacturers.name");
            String productCountry = resultSet.getString("Countries.name");

            returnProduct = new Product(
                    id, productName, productCategory,
                    productPrice, productManufacturingDate, productWarrantyDate,
                    productStock, Controller.getIdByManufacturer(productManufacturer),
                    Controller.getIdByCountry(productCountry));
            resultSet.close();
            return returnProduct;
        } else {
            return null;
        }
    }

    public static ArrayList<Product> searchProductsByManufacturer(String manufacturer) throws SQLException {
        //modify method to parse the manufacturerId into a String and compare it to it's paramenter

        String searchProductByManufacturerSQL = new String("select Products.id as ProductId," +
                "Products.name as ProductName, Products.price\n" +
                "from Products \n" +
                "join Manufacturers \n" +
                "on(Products.manufacturerId=Manufacturers.id) \n" +
                "where Manufacturers.name in (\"" + manufacturer + "\");");

        ArrayList<Product> products = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(searchProductByManufacturerSQL);

        while (resultSet.next()) {
            products.add(new Product(resultSet.getByte("ProductId"),resultSet.getString("ProductName"),"null",
                    resultSet.getShort("price"),null,null,(byte)-1,getIdByManufacturer(manufacturer),(byte)-1));
        }

        return products;
    }

    public static void sellProductById(byte id) throws SQLException {
        String getProductStockByIdSQL = new String("select stock from Products where id=" + id + ";");
        Statement statement = connection.createStatement();
        ResultSet productStock = statement.executeQuery(getProductStockByIdSQL);
        productStock.next();
        short newStock = (short) (productStock.getInt("stock") - 1);
        String sellProductSQL = new String("update Products set stock=" +
                newStock + " where id=" + id + ";");
        statement.execute(sellProductSQL);
        productStock.close();

    }

    public static void deleteProductById(byte id) throws SQLException {
        String deleteProductByIdSQL = new String("delete from Products where id=" + id + ";");
        Statement statement = connection.createStatement();
        statement.execute(deleteProductByIdSQL);
    }

    public static void reloadProductStockById(byte id) throws SQLException {
        String getProductStockByIdSQL = new String("select stock from Products where id=" + id + ";");
        Statement statement = connection.createStatement();
        ResultSet productStock = statement.executeQuery(getProductStockByIdSQL);
        productStock.next();
        short newStock = (short) (productStock.getInt("stock") + 1);
        String reloadProductStockSQL = new String("update Products set stock=" +
                newStock + " where id=" + id + ";");
        statement.execute(reloadProductStockSQL);
        productStock.close();
    }

    //    public static void checkForUniqueId(byte id) throws Exception {
//        String getProductIdsSQL = new String("select id from mydb.Products");
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery(getProductIdsSQL);
//
//        while (resultSet.next()) {
//            if (id == resultSet.getByte("id")) {
//                throw new Exception("Id " + id + " already exists in the DB!");
//            }
//        }
//        resultSet.close();
//    }

    public static byte getLastProductId() throws SQLException {
        String sql = new String("SELECT id FROM Products\n" +
                "ORDER BY id DESC\n" +
                "LIMIT 1;");
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        resultSet.first();
        return resultSet.getByte("id");
    }

    //    public static void getProductsPriceListByManufacturer(String manufacturer) throws SQLException {
//        String sql = new String("select Products.name, Products.price, Manufacturers.name as Manufacturer\n" +
//                "from Products \n" +
//                "join Manufacturers \n" +
//                "on Products.manufacturerId=Manufacturers.id\n" +
//                "where manufacturerId='1';");
//        // change manufacturer id with the one returned by parsing the name returning it's value
//    }

    public static ArrayList<Product> getProductsByCategory(String category) throws SQLException {
        String productsSQL = new String("select Products.id,Products.name,Products.price \n from Products\n where category='" + category + "';");
        ArrayList<Product> products = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(productsSQL);

        while (resultSet.next()) {
            products.add(new Product(resultSet.getByte("id"),resultSet.getString("name"),category,
                    resultSet.getShort("price"),null,null,(byte)-1,(byte)-1,(byte)-1));
        }

        return products;
    }

    public static short getProductsByCategoryPriceSum(String category) throws SQLException {
        String sumSQL = new String("select SUM(Products.price) as TotalPriceOfProducts from Products where category='" + category + "'");

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sumSQL);
        resultSet.next();

        return resultSet.getShort("TotalPriceOfProducts");
    }
}
