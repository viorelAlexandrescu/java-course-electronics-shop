/**
 * Created by viorel on 10.06.2016.
 */
public enum Categories {
    CPU("CPU"),GPU("GPU"),PERIPHERAL("Peripheral"),LAPTOP("Laptop");

    private final String categoryName;

    /**
     * It may be used as a placeholder for not querying the database every time you want to add a new product.
     *
     * @param newName sets the constant's new name
     */
    Categories(String newName){
        this.categoryName = newName;
    }

    public String getName(){
        return categoryName;
    }

    public static String[] getAllCategories(){
        return new String[]{CPU.getName(),GPU.getName(),PERIPHERAL.getName(),LAPTOP.getName()};
    }
}
