public enum Countries {
    BULGARY((byte) 1, "Bulgary"),
    GREAT_BRITAIN((byte) 2, "Great Britain"),
    GERMANY((byte) 3, "Germany"),
    ROMANIA((byte) 4, "Romania"),
    UCRAINE((byte) 5, "Ucraine"),
    ITALY((byte) 6, "Italy"),
    SWEDEN((byte) 7, "Sweden"),
    JAPAN((byte) 8, "Japan"),
    CHINA((byte) 9, "China");

    private byte id;
    private String name;

    /**
     * This enum is used to easily instantiate Product objects.
     * It may be used as a placeholder for not querying the database every time you want to add a new product.
     *
     * @param newId
     * @param newName
     */
    Countries(byte newId, String newName) {
        this.id = newId;
        this.name = newName;
    }

    public byte getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static String[] getAllCountries(){
        return new String[]{BULGARY.getName(),GREAT_BRITAIN.getName(),GERMANY.getName(),ROMANIA.getName(),
                UCRAINE.getName(),ITALY.getName(),SWEDEN.getName(),JAPAN.getName(),CHINA.getName()};
    }
}