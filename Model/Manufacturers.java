enum Manufacturers {
    INTEL((byte) 1, "Intel"),
    AMD((byte) 2, "AMD"),
    NVIDIA((byte) 3, "Nvidia"),
    LENOVO((byte) 4, "Lenovo"),
    SAMSUNG((byte) 5, "Samsung"),
    PHILLIPS((byte) 6, "Phillips"),
    GENIUS((byte) 7, "Genius"),
    ACER((byte) 8, "Acer"),
    APPLE((byte) 9, "Apple Inc.");

    private final byte id;
    private final String name;

    /**
     * This enum is used to easily instantiate Product objects.
     * Also when searching for products by a certain criteria (e.g. by the manufacturers name or id)
     * and when selecting by which manufacturer is the product made
     *
     * @param newId   this is the direct corespondent to the manufacturer's name
     * @param newName this is the manufacturer's name
     */
    Manufacturers(byte newId, String newName) {
        this.id = newId;
        this.name = newName;
    }

    public byte getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static String[] getAllManufacturers(){
        return new String[]{INTEL.getName(),AMD.getName(),NVIDIA.getName(),LENOVO.getName(),
        SAMSUNG.getName(),PHILLIPS.getName(),GENIUS.getName(),ACER.getName(),APPLE.getName()};
    }
}