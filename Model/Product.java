import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Product{
    private byte id;
    private String name;
    private String category;
    private short price;
    private Date manufacturingDate;
    private Date warranty;
    private byte manufacturerId;
    private byte countryId;
    private short stock;

    /**
     * This is the class represents a Product entity. Via objects like these, we can instantitate data
     * from the database.
     * <p>
     * This constructor is used to manually instantiate the data,
     * via direct user input or by making a callback and pulling data from the database.
     *
     * @param newId                the unique id value
     * @param newName              new product's name
     * @param newCategory          new product's category
     * @param newPrice             new product's price
     * @param newManufacturingDate when the new product was made
     * @param newWarranty          this is for how long the product is in warranty
     * @param newStock             how many pieces of this product exist
     * @param newManufacturerId    this id is a direct corespondent of the product's manufacturer
     * @param newCountryId         this id directly represents the country where the product was made
     */
    public Product(byte newId, String newName, String newCategory,
                   short newPrice, Date newManufacturingDate, Date newWarranty, short newStock,
                   byte newManufacturerId, byte newCountryId){
        this.id = newId;
        this.name = newName;
        this.category = newCategory;
        this.price = newPrice;
        this.manufacturingDate = newManufacturingDate;
        this.warranty = newWarranty;
        this.stock = newStock;
        this.manufacturerId = newManufacturerId;
        this.countryId = newCountryId;
    }

    /**
     * This is the class represents a Product entity. Via objects like these, we can instantitate data
     * from the database.
     *
     * This constructor is used to automatically load data from a CSV file and instantiate a new Product object
     * @param filePath the absolute path to the file
     * @throws IOException when the file does not exist or the path is wrong
     */
    public Product(String filePath)throws IOException{
        filePath = new String("file://" + filePath);
        File fileInput = new File(URI.create(filePath));
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileInput));
        String line, data[] = new String[9];

        while((line=bufferedReader.readLine()) != null){
            data = line.split(",");
        }
        if(data != null){
            this.id = Byte.parseByte(data[0]);
            this.name = data[1];
            this.category = data[2];
            this.price = Short.parseShort(data[3]);
            this.manufacturingDate = Date.valueOf(data[4]);
            this.warranty = Date.valueOf(data[5]);
            this.manufacturerId = Controller.getIdByManufacturer(data[6]);
            this.countryId = Controller.getIdByCountry(data[7]);
            this.stock = Short.parseShort(data[8]);
        }
        else {
            throw new IOException("No input from file");
        }
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public short getPrice() {
        return price;
    }

    public void setPrice(short price) {
        this.price = price;
    }

    public Date getManufacturingYear() {
        return manufacturingDate;
    }

    public void setManufacturingYear(Date manufacturingYear) {
        this.manufacturingDate = manufacturingYear;
    }

    public Date getWarranty() {
        return warranty;
    }

    public void setWarranty(Date warranty) {
        this.warranty = warranty;
    }

    public short getStock() {
        return stock;
    }

    public void setStock(short stock) {
        this.stock = stock;
    }

    public byte getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(byte manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public byte getCountryId() {
        return countryId;
    }

    public void setCountryId(byte countryId) {
        this.countryId = countryId;
    }

    /**
     * @return returns a string representation of all the data contained within the Product's fields
     */
    @Override
    public String toString(){
        return "\nid:" +
                id + "\nname:" +
                name + "\ncategory:" +
                category + "\nprice:" +
                price + "\nmanufacturingDate:" +
                manufacturingDate + "\nwarranty:" +
                warranty + "\nstock:" +
                stock + "\nmanufacturerId:" +
                manufacturerId + "\ncountryId:" +
                countryId + "\n";
    }

}