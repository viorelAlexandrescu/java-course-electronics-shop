import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;;import java.io.InputStream;

public class MainPage extends Application {

    private static Parent rootParent;
    private static Stage mainStage;

    @Override
    public void start(Stage primaryStage){
        try {
            Controller controller = new Controller();
            rootParent = FXMLLoader.load(getClass().getResource("util/MainPage.fxml"));
            mainStage = new Stage();
            Scene scene = new Scene(rootParent);
            mainStage.setTitle("Electronics Shop");
            mainStage.setScene(scene);
            mainStage.show();
        } catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println("Please try again later");
            System.exit(1);
        }
    }

    static Parent getRootParent() {
        return rootParent;
    }

    static Stage getMainStage() {
        return mainStage;
    }

    @Override
    public void stop() throws Exception{
        System.out.println("App closing...");
        Controller.getConnection().close();
        System.out.println("Connection to DB closed.");
        System.out.println("Bye");
    }

    public static void main(String[] args) {
        launch();
    }

}