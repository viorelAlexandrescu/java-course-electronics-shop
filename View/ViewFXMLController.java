import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ViewFXMLController implements Initializable {
    private static Parent secondParent;
    private static Stage secondStage = new Stage();
    private static String selection;

    public static Parent getSecondParent() {
        return secondParent;
    }

    public static void setSecondParent(Parent secondParent) {
        ViewFXMLController.secondParent = secondParent;
    }

    public static Stage getSecondStage() {
        return secondStage;
    }

    public static void setSecondStage(Stage secondStage) {
        ViewFXMLController.secondStage = secondStage;
    }

    @FXML
    ListView<String> categoryListView = new ListView<>();

    @FXML
    Label errorOnSearchProductById_Label, errorOnSearchProductByManufacturer_Label;

    @FXML
    TextField searchProductById_TextField, searchProductByManufacturer_TextField;


    @FXML
    public void onInsertNewProductButtonClick() throws Exception {
        secondParent = FXMLLoader.load(getClass().getResource("util/SelectInsertMethod.fxml"));
        Scene insertScene = new Scene(secondParent);
        secondStage.setScene(insertScene);
        secondStage.show();
    }

    @FXML
    private void onSearchProductByIdButtonClick(Event e) throws Exception {
        if (searchProductById_TextField.getText().isEmpty()) {
            errorOnSearchProductById_Label.setText("Please insert an id");
        }
        errorOnSearchProductById_Label.setText("");
        try {
            Product returnProduct;
            returnProduct = Controller.searchProductById(Byte.parseByte(searchProductById_TextField.getText()));
            if (returnProduct == null) {
                errorOnSearchProductById_Label.setText("There is no product by that id");
            } else {
                GetProductDetailsController.setProduct(returnProduct);
                secondParent = FXMLLoader.load(getClass().getResource("util/GetProductDetails.fxml"));
                Scene getProductScene = new Scene(secondParent);
                secondStage.setScene(getProductScene);
                secondStage.show();
            }
        } catch (Exception ex) {
            errorOnSearchProductById_Label.setText("There is no product by that id");
//            ex.printStackTrace();
        }
    }

    @FXML
    private void onSearchProductByManufacturerButtonClick(Event e) throws Exception {
        if (searchProductByManufacturer_TextField.getText().isEmpty()) {
            errorOnSearchProductByManufacturer_Label.setText("Please insert a name");
        } else {
            errorOnSearchProductById_Label.setText("");
            SearchByManufacturerController.setManufacturer(searchProductByManufacturer_TextField.getText());
            SearchByManufacturerController.setProducts(
                    Controller.searchProductsByManufacturer(searchProductByManufacturer_TextField.getText()));
            secondParent = FXMLLoader.load(getClass().getResource("/SearchProductsByManufacturer.fxml"));
            Scene getProductByManufacturerScene = new Scene(secondParent);
            secondStage.setScene(getProductByManufacturerScene);
            secondStage.show();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String[] listViewCategories = Categories.getAllCategories();

        for (int i = 0; i < listViewCategories.length; i++) {
            listViewCategories[i] = listViewCategories[i].concat("s");
        }

        ObservableList<String> categoriesList = FXCollections.observableArrayList(
                listViewCategories
        );
        categoryListView.setItems(categoriesList);
        categoryListView.setCellFactory(ComboBoxListCell.forListView(categoriesList));

        categoryListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                for (String s : Categories.getAllCategories()) {
                    if (newValue.contains(s)) {
                        selection = s;
                        break;
                    }
                }
                try {
                    SearchByCategoryController.setCategory(selection);
                    SearchByCategoryController.setProducts(Controller.getProductsByCategory(selection));
                    SearchByCategoryController.setTotalPriceOfProductsByCategory(Controller.getProductsByCategoryPriceSum(selection));
                    secondParent = FXMLLoader.load(getClass().getResource("util/ShowProductsByCategory.fxml"));
                    Scene scene = new Scene(secondParent);
                    secondStage.setScene(scene);
                    secondStage.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
