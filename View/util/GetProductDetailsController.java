import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by viorel on 10.06.2016.
 */
public class GetProductDetailsController implements Initializable {

    private static Product product;

    @FXML
    public Label productIdLabel, productNameLabel, productCategoryLabel, productStockLabel,
            productPriceLabel, productManufacturingDateLabel, productWarrantyLabel,
            productManufacturerLabel, productCountryLabel;

    static void setProduct(Product newProduct) {
        product = newProduct;
    }

    @FXML
    private void sellProductButtonClick() {
        try {
            Controller.sellProductById(product.getId());
            product.setStock((short) (product.getStock() - 1));
            productStockLabel.setText("Stock:" + product.getStock());
            System.out.println("Sold 1 piece");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteProductButtonClick() {
        try {
            Controller.deleteProductById(product.getId());
            System.out.println("Product with id(" + product.getId() + ") has been deleted.");
            ViewFXMLController.getSecondStage().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void loadStockButtonClick() {
        try {
            Controller.reloadProductStockById(product.getId());
            product.setStock((short) (product.getStock() + 1));
            productStockLabel.setText("Stock:" + product.getStock());
            System.out.println("Product stock updated");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showData() {
        productIdLabel.setText(productIdLabel.getText().concat(String.valueOf(product.getId())));
        productNameLabel.setText(productNameLabel.getText().concat(product.getName()));
        productCategoryLabel.setText(productCategoryLabel.getText().concat(product.getCategory()));
        productPriceLabel.setText(productPriceLabel.getText().concat(String.valueOf(product.getPrice())) + " RONs");
        productManufacturingDateLabel.setText(productManufacturingDateLabel.getText().concat(product.getManufacturingYear().toString()));
        productWarrantyLabel.setText(productWarrantyLabel.getText().concat(product.getWarranty().toString()));
        productManufacturerLabel.setText(productManufacturerLabel.getText().concat(Controller.getManufacturerById(product.getManufacturerId())));
        productCountryLabel.setText(productCountryLabel.getText().concat(Controller.getCountryById(product.getCountryId())));
        productStockLabel.setText(productStockLabel.getText().concat(String.valueOf(product.getStock())));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showData();
    }
}
