import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.print.URIException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class InsertProductController implements Initializable {

    ObservableList<String> manufacturersList = FXCollections.observableArrayList(Manufacturers.getAllManufacturers()),
            countriesList = FXCollections.observableArrayList(Countries.getAllCountries()),
            categoriesList = FXCollections.observableArrayList(Categories.getAllCategories());

    @FXML
    Label fileInputError;

    @FXML
    TextField productNameTextField, priceTextField, numberOfProductsTextField, inputFilePath;

    @FXML
    DatePicker manufacturingDateDatePicker, warrantyDatePicker;

    @FXML
    ComboBox<String> manufacturerComboBox = new ComboBox<>(),
            categoryComboBox = new ComboBox<>(),
            manufacturingCountryComboBox = new ComboBox<>();

    @FXML
    CheckBox showFileInputCheckBox;

    @FXML
    public void onSelectInputMethodButtonClick(Event event) {
        Button selection = (Button) event.getSource();
        try {
            if (selection.getId().equals("userInput")) {

                ViewFXMLController.setSecondParent(FXMLLoader.load(getClass().getResource("util/InsertProductFromUserInput.fxml")));
                Scene userInputScene = new Scene(ViewFXMLController.getSecondParent());
                ViewFXMLController.getSecondStage().setScene(userInputScene);
                ViewFXMLController.getSecondStage().show();

            }
            if (selection.getId().equals("fileInput")) {

                ViewFXMLController.setSecondParent(FXMLLoader.load(getClass().getResource("util/InsertProductFromFile.fxml")));
                Scene userInputScene = new Scene(ViewFXMLController.getSecondParent());
                ViewFXMLController.getSecondStage().setScene(userInputScene);
                ViewFXMLController.getSecondStage().show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onInsertNewData(Event event) {
        Button inputType = (Button) event.getSource();
        Product newProduct;
        try {
            if (inputType.getId().equals("insertFromUser")) {
                newProduct = new Product(
                        (byte) (Controller.getLastProductId() + 1),
                        productNameTextField.getText(),
                        categoryComboBox.getValue(),
                        Short.parseShort(priceTextField.getText()),
                        Date.valueOf(manufacturingDateDatePicker.getValue()),
                        Date.valueOf(warrantyDatePicker.getValue()),
                        Short.parseShort(numberOfProductsTextField.getText()),
                        Controller.getIdByManufacturer(manufacturerComboBox.getValue()),
                        Controller.getIdByCountry(manufacturingCountryComboBox.getValue())
                );

                Controller.addProduct(newProduct);
                System.out.println("Succesfully added product " + newProduct.getName() +
                        " with id:" + newProduct.getId());
                ViewFXMLController.getSecondStage().close();
            }
            if (inputType.getId().equals("insertFromFile")) {
                newProduct = new Product(inputFilePath.getText());
                if (showFileInputCheckBox.isSelected()) {
                    GetProductDetailsController.setProduct(newProduct);
                    Parent parent = FXMLLoader.load(getClass().getResource("util/ShowProductDetails.fxml"));
                    Stage showFileInputStage = new Stage();
                    Scene getProductScene = new Scene(parent);
                    showFileInputStage.setScene(getProductScene);
                    showFileInputStage.show();
                }
                Controller.addProduct(newProduct);
                System.out.println("Succesfully added product " + newProduct.getName() +
                        " with id:" + newProduct.getId());
                ViewFXMLController.getSecondStage().close();
            }

        } catch (Exception e) {
            System.err.println("Failed to add a new product");
            if(e instanceof IllegalArgumentException){
                fileInputError.setText("Wrong path or no file found");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        manufacturerComboBox.setItems(manufacturersList);
        manufacturingCountryComboBox.setItems(countriesList);
        categoryComboBox.setItems(categoriesList);
    }
}
