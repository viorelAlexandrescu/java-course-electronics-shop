import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.ComboBoxListCell;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class SearchByCategoryController implements Initializable {
    private static String category;
    private static short totalPrice;
    private static ArrayList<Product> products;

    @FXML
    Label categoryLabel, totalPriceOfProductsSale;

    @FXML
    ListView productsListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        categoryLabel.setText(categoryLabel.getText().concat(category));

        ObservableList<String> productsList = FXCollections.observableArrayList(
                products.toString()
        );

        productsListView.setItems(productsList);
        productsListView.setCellFactory(ComboBoxListCell.forListView(productsList));

        totalPriceOfProductsSale.setText(totalPriceOfProductsSale.getText().concat(totalPrice + " RONs"));
    }

    public static void setCategory(String newCategory){
        category = newCategory;
    }

    public static void setTotalPriceOfProductsByCategory(short newPrice){
        totalPrice = newPrice;
    }

    public static void setProducts(ArrayList<Product> newProducts){
        products = newProducts;
    }
}
