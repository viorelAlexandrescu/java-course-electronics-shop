import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.ComboBoxListCell;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by viorel on 14.06.2016.
 */
public class SearchByManufacturerController implements Initializable {


    private static ArrayList<Product> products;

    public static void setManufacturer(String newManufacturer) {
        manufacturer = newManufacturer;
    }

    private static String manufacturer;

    @FXML
    Label manufacturerLabel;

    @FXML
    ListView<String> productsListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> productsElements = FXCollections.observableArrayList(
                products.toString()
        );

        productsListView.setItems(productsElements);
        productsListView.setCellFactory(ComboBoxListCell.forListView(productsElements));

        manufacturerLabel.setText(manufacturerLabel.getText().concat(manufacturer));

    }

    public static void setProducts(ArrayList<Product> newProducts) {
        products = newProducts;
    }
}

